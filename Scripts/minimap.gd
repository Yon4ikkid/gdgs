extends Control

func initialize(grid: Array):
	for row in grid.size():
		for col in grid.size():
			match grid[row][col]:
				"S":
					$TileMap.set_cell(0, Vector2i(col, row), 0, Vector2i(1,0))
				"@":
					$TileMap.set_cell(0, Vector2i(col, row), 0, Vector2i(1,1))
				"M":
					$TileMap.set_cell(0, Vector2i(col, row), 0, Vector2i(0,0))
				"B":
					$TileMap.set_cell(0, Vector2i(col, row), 0, Vector2i(2,0))
				"T":
					$TileMap.set_cell(0, Vector2i(col, row), 0, Vector2i(0,1))
	$Cursor.position = Vector2(120, 120)

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
