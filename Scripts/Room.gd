extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_doors(con: Array):
	$Fader/AnimationPlayer.play("fade_in")
	if con[0] == 1:
		$Doors/DoorUp.show()
	else:
		$Doors/DoorUp.hide()
	if con[1] == 1:
		$Doors/DoorRight.show()
	else:
		$Doors/DoorRight.hide()
	if con[2] == 1:
		$Doors/DoorDown.show()
	else:
		$Doors/DoorDown.hide()
	if con[3] == 1:
		$Doors/DoorLeft.show()
	else:
		$Doors/DoorLeft.hide()
	$Fader/AnimationPlayer.play("fade_out")
