extends CharacterBody2D

const SPEED = 300.0
var animation_tree = null
var state_machine = null

func _ready():
	animation_tree =  $AnimationTree
	state_machine = animation_tree.get("parameters/playback")

func _physics_process(delta):
	var horizontal_direction = Input.get_axis("ui_left", "ui_right")
	var vertical_direction = Input.get_axis("ui_up", "ui_down")
	
	if horizontal_direction:
		velocity.x = horizontal_direction * SPEED
		if horizontal_direction < 0:
			state_machine.travel("left")
		else:
			state_machine.travel("right")
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
	
	if vertical_direction:
		velocity.y = vertical_direction * SPEED
		if vertical_direction < 0:
			state_machine.travel("backward")
		else:
			state_machine.travel("forward")
	else:
		velocity.y = move_toward(velocity.y, 0, SPEED)	
	
	if not vertical_direction and not horizontal_direction:
		state_machine.travel("idle")
	
	# look_at(get_global_mouse_position())
	move_and_slide()
