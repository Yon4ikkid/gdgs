extends Node

const _grid_side: int = 15
const room_sign: String = "@"
const empty_sign: String = " "
const special_signs: Array = ["B","T","M"]
var grid: Array = []
var current_position: Vector2i = Vector2i(_grid_side / 2, _grid_side / 2)

func _ready():
	_initialize_grid()

func _initialize_grid():
	for x in range(_grid_side):
		var row = []
		row.resize(_grid_side)
		row.fill(empty_sign)
		grid.append(row)

func _print_grid():
	var rstr: String
	for row in grid:
		rstr = ""
		for val in row:
			rstr += val
		print(rstr)

func _is_cell_suitable(row: int, col: int) -> bool:
	var count: int = _count_connected_neighbors(row, col)
	return count == 1

func _count_connected_neighbors(row: int, col: int) -> int:
	var count: int = 0
	if row + 1 < _grid_side and grid[row + 1][col] == room_sign:
		count += 1
	if row - 1 >= 0 and grid[row - 1][col] == room_sign:
		count += 1
	if col + 1 < _grid_side and grid[row][col + 1] == room_sign:
		count += 1
	if col - 1 >= 0 and grid[row][col - 1] == room_sign:
		count += 1
	return count

func _determine_change(r: int, c: int) -> int:
	if grid[r][c] in special_signs:
		return 0
	var change: int = 0
	var connections: int = 0
	for i in range(-1, 2, 2):
		if grid[r + i][c] != empty_sign:
			continue
		connections = _count_connected_neighbors(r + i, c)
		if connections == 1:
			change += 1
		elif connections > 1:
			change -= 1

	for j in range(-1, 2, 2):
		if grid[r][c + j] != empty_sign:
			continue
		connections = _count_connected_neighbors(r, c + j)
		if connections == 1:
			change += 1
		elif connections > 1:
			change -= 1
	
	return change

func _generate_iteration(available_cells: int, rng: RandomNumberGenerator, sign: String) -> int:
	var attempts: int = 1
	for r in range(1, _grid_side - 1):
		for c in range(1, _grid_side - 1):
			if grid[r][c] == empty_sign and _is_cell_suitable(r, c):
				if attempts == available_cells or rng.randi_range(1, available_cells) == 1:
					grid[r][c] = sign
					var change = _determine_change(r, c)
					return available_cells - 1 + change
				else:
					attempts += 1
	return 4

func _get_current_cell_connections() -> Array:
	var connections: Array = [0,0,0,0]
	if grid[current_position.y - 1][current_position.x] != empty_sign:
		connections[0] = 1
	if grid[current_position.y][current_position.x + 1] != empty_sign:
		connections[1] = 1
	if grid[current_position.y + 1][current_position.x] != empty_sign:
		connections[2] = 1
	if grid[current_position.y][current_position.x - 1] != empty_sign:
		connections[3] = 1
	return connections

func generate_map(room_count: int, se: int) -> Array:
	var _room_stack: Array = special_signs.duplicate()
	for i in range(room_count):
		_room_stack.push_back(room_sign)
	grid[_grid_side / 2][_grid_side / 2] = room_sign
	var available_cells: int = 4
	var rng = RandomNumberGenerator.new()
	rng.seed = se
	while (_room_stack.is_empty() == false):
		available_cells = _generate_iteration(available_cells, rng, _room_stack.pop_back())
	grid[_grid_side / 2][_grid_side / 2] = "S"
	$Minimap.initialize(grid)
	return _get_current_cell_connections()

func move(dir: String) -> Array:
	match dir:
		"Up":
			current_position.y -= 1
		"Down":
			current_position.y += 1
		"Left":
			current_position.x -= 1
		"Right":
			current_position.x += 1
	$Minimap/Cursor.position.x = current_position.x * 16 + 8
	$Minimap/Cursor.position.y = current_position.y * 16 + 8
	return _get_current_cell_connections()
