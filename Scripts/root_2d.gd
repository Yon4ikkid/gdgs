extends Node2D

func _ready():
	
	var con: Array = $MapController.generate_map(8,77)
	$Room.set_doors(con)

func _on_door_up_body_entered(body):
	var con: Array = $MapController.move("Up")
	$Room.set_doors(con)
	$Player.position.y = $Room/Doors/DoorDown.position.y - 35

func _on_door_down_body_entered(body):
	var con: Array = $MapController.move("Down")
	$Room.set_doors(con)
	$Player.position.y = $Room/Doors/DoorUp.position.y + 35

func _on_door_left_body_entered(body):
	var con: Array = $MapController.move("Left")
	$Room.set_doors(con)
	$Player.position.x = $Room/Doors/DoorRight.position.x - 35

func _on_door_right_body_entered(body):
	var con: Array = $MapController.move("Right")
	$Room.set_doors(con)
	$Player.position.x = $Room/Doors/DoorLeft.position.x + 35
